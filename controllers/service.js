const Service = require('../models/service');

exports.getAllServices = async (req, res, next) => {
    try {
        const [allServices] = await Service.fetchAll();
        res.status(200).json(allServices);
    } catch (err){
        if(!err.statusCode){
            err.statusCode = 500
        }
        next(err);
    }
};
exports.postServices = async (req, res, next) => {
    try {
        const postResponse = await Service.post(req.body.item);
        res.status(201).json(postResponse);
    } catch (err){
        if(!err.statusCode){
            err.statusCode = 500
        }
        next(err);
    }
};

exports.putServices = async (req, res, next) => {
    try {
        const putResponse = await Service.update(req.body.id, req.body.item);
        res.status(200).json(putResponse);
    } catch (err){
        if(!err.statusCode){
            err.statusCode = 500
        }
        next(err);
    }
};

exports.deleteServices = async (req, res, next) => {
    try {
        const deleteResponse = await Service.delete(req.params.id);
        res.status(200).json(deleteResponse);
    } catch (err){
        if(!err.statusCode){
            err.statusCode = 500
        }
        next(err);
    }
};
