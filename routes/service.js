const express = require('express');

const serviceController = require('../controllers/service');

const router = express.Router();

router.get('/', serviceController.getAllServices);

router.post('/', serviceController.postServices);

router.put('/', serviceController.putServices);

router.delete('/:id', serviceController.deleteServices);

module.exports = router;