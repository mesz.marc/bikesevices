const db = require('../util/database')

module.exports = class Service {
    constructor(id, item) {
        this.id = id;
        this.item = item;
    }
    static fetchAll(){
        return db.execute('SELECT * FROM services');
    }
    static post(item) {
        return db.execute('INSERT INTO services (item) VALUES (?)', [item]);
    }
    static update(id, item) {
        return db.execute('UPDATE services SET item = ? WHERE id = ?', [item, id]);
    }
    static delete(id) {
        return db.execute('DELETE FROM services WHERE id = ?', [id]);
    }
};